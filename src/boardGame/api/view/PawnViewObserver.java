package boardGame.api.view;

public interface PawnViewObserver {

	void onChange(PawnView pawnView);
}
