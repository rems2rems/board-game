package boardGame.api.view;

import boardGame.api.model.Pawn;

public interface BoardView extends SquareViewObserver,PawnViewObserver {
	
	void addSquareView(SquareView squareView);
	void addPawnView(PawnView pawnView);
	void removePawn(Pawn pawn);
	
}
