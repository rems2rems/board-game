package boardGame.api.model;

public interface SquareObserver {

	void onPawnAdded(Square source,Pawn pawn);
	void onPawnRemoved(Square source,Pawn pawn);
    void onStateChanged(Square source);
}
