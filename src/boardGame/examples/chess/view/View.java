package boardGame.examples.chess.view;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import boardGame.api.controller.ControllerObserver;
import boardGame.api.model.Action;
import boardGame.api.model.Board;
import boardGame.api.model.Direction;
import boardGame.api.model.Pawn;
import boardGame.api.view.PlayerObserver;
import boardGame.api.view.SquareView;
import boardGame.examples.chess.controller.Controller;
import boardGame.examples.chess.model.Piece;
import boardGame.view.BoardPanel;
import boardGame.view.CursorView;
import boardGame.view.SimpleKeyboardManager;

public class View extends JFrame implements PlayerObserver, ControllerObserver {

	private static final long serialVersionUID = -649678480168029763L;
	
	Controller controller;
	BoardPanel game;

	public View(String title, Controller controller, Board board)  {
		super(title);
		this.controller = controller;
		controller.addObserver(this);
		int squareHeight = 50;
		int squareWidth = 50;
		
		game = new BoardPanel(board, squareHeight,squareWidth);
		if(board.hasCursor()) {
			game.addPawnView(new CursorView(board.getCursor(),squareHeight,squareWidth));
		}
		for (int row = 0; row < board.getNbRows(); row++) {
			for (int col = 0; col < board.getNbCols(); col++) {
				SquareView view = new CheckerSquareView(board.getSquare(row, col),squareHeight,squareWidth);
				view.setSprite(null);
				if((row+col)%2 == 0) {
					view.setColor(Color.WHITE);
				}
				else {
					view.setColor(Color.GRAY);
				}
				game.addSquareView(view);
				for (Pawn pawn : board.getSquare(row, col).getPawns()) {
					if(pawn == board.getCursor()) {
						continue;
					}
					Piece piece = (Piece)pawn;
					game.addPawnView(new PieceView(piece,squareHeight,squareWidth));
				}
			}
		}
				
		
		SimpleKeyboardManager keyboardManager = new SimpleKeyboardManager();
		addKeyListener(keyboardManager);
		keyboardManager.addObserver(0, this);

		setLayout(new BorderLayout());
		getContentPane().add(game);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		getContentPane().setSize(game.getSize());
		pack();
		setVisible(true);
	}

	@Override
	public void onAction(int playerNbr, Action action, Direction direction) {
		
		if(action == Action.MOVE) {
			controller.moveCursor(direction);
		}
		else {
			controller.toggleSelect();
		}
	}

	@Override
	public void notifyGameIsOver(String message) {
		
		JOptionPane.showMessageDialog(game, message);
		System.exit(0);
	}
}
