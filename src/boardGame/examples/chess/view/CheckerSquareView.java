package boardGame.examples.chess.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import boardGame.api.model.Square;
import boardGame.examples.chess.model.CheckerSquare;
import boardGame.view.SquareViewImpl;

public class CheckerSquareView extends SquareViewImpl {

	private static final long serialVersionUID = 2330252359129681209L;

	private Color selectedPieceColor = new Color(Color.GREEN.getRed(), Color.GREEN.getGreen(), Color.GREEN.getBlue(),
			125);
	private Color possibleMoveColor = new Color(Color.BLUE.getRed(), Color.BLUE.getGreen(), Color.BLUE.getBlue(), 125);
	private Color possibleAttack = new Color(Color.ORANGE.getRed(), Color.ORANGE.getGreen(), Color.ORANGE.getBlue(),
			125);

	public CheckerSquareView(Square square, int squareHeight, int squareWidth) {
		super(square, squareHeight, squareWidth);
	}

	public Color getPossibleMoveColor() {
		return possibleMoveColor;
	}

	public void setPossibleMoveColor(Color possibleMoveColor) {
		this.possibleMoveColor = possibleMoveColor;
	}

	public Color getSelectedPieceColor() {
		return selectedPieceColor;
	}

	public void setSelectedPieceColor(Color selectedPieceColor) {
		this.selectedPieceColor = selectedPieceColor;
	}

	public Color getPossibleAttack() {
		return possibleAttack;
	}

	public void setPossibleAttack(Color possibleAttack) {
		this.possibleAttack = possibleAttack;
	}

	@Override
	public void paint(Graphics g) {

		super.paint(g);

		Graphics2D graphics = (Graphics2D) g;
		CheckerSquare square = (CheckerSquare) getSquare();
		Color color=null;
		if (square.isPossibleMove() && !square.isSelectedPiece()) {
			color = getPossibleMoveColor();
		}
		if (square.isSelectedPiece()) {
			color = getSelectedPieceColor();
		}
		if (square.isPossibleAttack()) {
			color = getPossibleAttack();
		}

		if (color != null) {
			graphics.setPaint(color);
			graphics.fillRect(getSquare().getColumn() * squareWidth, getSquare().getRow() * squareHeight, squareWidth,
					squareHeight);
		}

	}
}
