package boardGame.examples.chess.view;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import boardGame.api.model.Pawn;
import boardGame.examples.chess.model.Piece;
import boardGame.view.PawnViewImpl;

public class PieceView extends PawnViewImpl {
	
	private static final long serialVersionUID = 606342603211102493L;

	public PieceView(Pawn pawn, int squareHeight,int squareWidth) {
		super(pawn,squareHeight,squareWidth);
		Piece piece = (Piece)pawn;
		try {
			Image sprite = ImageIO.read(new File("images/chess/" + piece.getColor().name().toLowerCase() + "_" + piece.getType().name().toLowerCase() + ".png"));
			setSprite(sprite);
		} catch (IOException e) {
			throw new RuntimeException("sprite not found");
		}
	}
	
}
