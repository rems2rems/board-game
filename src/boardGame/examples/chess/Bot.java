package boardGame.examples.chess;

import java.util.Random;
import java.util.Set;

import boardGame.examples.chess.model.Checker;
import boardGame.examples.chess.model.CheckerSquare;
import boardGame.examples.chess.model.Color;
import boardGame.examples.chess.model.Piece;

public class Bot {
	
	private Checker board;
	private Color myColor;
	
	public Bot(Checker board, Color myColor) {
		super();
		this.board = board;
		this.myColor = myColor;
	}
	
	//TODO minimax !
	/**
	 * 
	 * https://en.wikipedia.org/wiki/Minimax
	 * 
	 
	   function minimax(node, depth, maximizingPlayer) is
		    if depth = 0 or node is a terminal node then
		        return the heuristic value of node
		    if maximizingPlayer then
		        value := −∞
		        for each child of node do
		            value := max(value, minimax(child, depth − 1, FALSE))
		        return value
		    else (* minimizing player *)
		        value := +∞
		        for each child of node do
		            value := min(value, minimax(child, depth − 1, TRUE))
		        return value
		        
		(* Initial call *)
		minimax(origin, depth, TRUE)
	        
	 * 
	 */
	//TODO : minimax + alphabeta pruning
	/**
	 * 
	   
	   function alphabeta(node, depth, α, β, maximizingPlayer) is
		    if depth = 0 or node is a terminal node then
		        return the heuristic value of node
		    if maximizingPlayer then
		        value := −∞
		        for each child of node do
		            value := max(value, alphabeta(child, depth − 1, α, β, FALSE))
		            α := max(α, value)
		            if α ≥ β then
		                break (* β cut-off *)
		        return value
		    else
		        value := +∞
		        for each child of node do
		            value := min(value, alphabeta(child, depth − 1, α, β, TRUE))
		            β := min(β, value)
		            if α ≥ β then
		                break (* α cut-off *)
		        return value
		        
		(* Initial call *)
		alphabeta(origin, depth, −∞, +∞, TRUE)
		        
	 * @return
	 */
	public ChessPlay play() {
		
		Set<Piece> pieces = board.getPieces(myColor);
		Piece piece=null;
		CheckerSquare to;
		


		
		do {
		
			
			do {
				piece = pieces.toArray(new Piece[0])[new Random().nextInt(pieces.size())];
			}
			while(!piece.hasPossibleMove());
			
			Set<CheckerSquare> moves = piece.getAllMoves();
			to = moves.toArray(new CheckerSquare[0])[new Random().nextInt(moves.size())];
		}
		while(!board.isValidMove(piece, to));
		
		return new ChessPlay((CheckerSquare)piece.getSquare(), to);
	}
}
