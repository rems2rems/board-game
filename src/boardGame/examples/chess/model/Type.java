package boardGame.examples.chess.model;

public enum Type {

	PAWN(1),BISHOP(3),KNIGHT(3),ROOK(4),QUEEN(8),KING(10);
	
	private int value;

	private Type(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
}
