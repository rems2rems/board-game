package boardGame.examples.chess.model.pieces;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import boardGame.api.model.Direction;
import boardGame.api.model.Square;
import boardGame.examples.chess.model.CheckerSquare;
import boardGame.examples.chess.model.Color;
import boardGame.examples.chess.model.Piece;
import boardGame.examples.chess.model.Type;

public class Queen extends Piece {

	public Queen(Color color) {
		super(Type.QUEEN, color);
	}

	@Override
	public Map<Direction, Set<CheckerSquare>> getAttacksByDirection() {

		Map<Direction, Set<CheckerSquare>> attacks = new HashMap<>();

		for (Direction direction : getSquare().getNeighbors().keySet()) {
			Set<CheckerSquare> moves = new HashSet<>();
			try {
				Square tmp = getSquare().getNeighbor(direction);
				while (!tmp.hasPawn()) {
					tmp = tmp.getNeighbor(direction);
				}

				Piece tmpPiece = ((CheckerSquare)tmp).getPiece();
				if (tmpPiece.getColor() != color) {
					moves.add((CheckerSquare) tmpPiece.getSquare());
				}
			} catch (Exception e) {
			}
			attacks.put(direction, moves);
		}
		return attacks;
	}

	@Override
	public Map<Direction, Set<CheckerSquare>> getFreeMovesByDirection() {
		Map<Direction, Set<CheckerSquare>> movesMap = new HashMap<>();
		for (Direction direction : getSquare().getNeighbors().keySet()) {
			Set<CheckerSquare> moves = new HashSet<>();
			try {
				Square tmp = getSquare().getNeighbor(direction);
				while (!tmp.hasPawn()) {
					moves.add((CheckerSquare) tmp);
					tmp = tmp.getNeighbor(direction);
				}
			} catch (Exception e) {
			}
			movesMap.put(direction, moves);
		}
		return movesMap;
	}
}
