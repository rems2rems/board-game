package boardGame.examples.chess.model;

import java.util.Set;

import boardGame.api.model.Pawn;
import boardGame.api.model.SquareObserver;
import boardGame.model.SquareImpl;

public class CheckerSquare extends SquareImpl {

	private boolean isPossibleMove = false;
	private boolean isSelectedPiece = false;
	private boolean isPossibleAttack = false;
	
	public CheckerSquare(Pawn cursor) {
		super(cursor);
	}
	
	public boolean isPossibleMove() {
		return isPossibleMove;
	}
	public void setPossibleMove(boolean isPossibleMove) {
		this.isPossibleMove = isPossibleMove;
		for (SquareObserver observer : observers) {
			observer.onStateChanged(this);
		}
	}
	public boolean isSelectedPiece() {
		return isSelectedPiece;
	}
	public void setSelectedPiece(boolean isSelectedPiece) {
		this.isSelectedPiece = isSelectedPiece;
		for (SquareObserver observer : observers) {
			observer.onStateChanged(this);
		}
	}
	
	public Piece getPiece() {
		
		return ((Set<Piece>)getPawns()).iterator().next();
	}

	public boolean isPossibleAttack() {
		return isPossibleAttack;
	}

	public void setPossibleAttack(boolean isPossibleAttack) {
		this.isPossibleAttack = isPossibleAttack;
	}
	
	@Override
	public String toString() {
		return "  ";
	}
}
