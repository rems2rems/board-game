package boardGame.examples.chess;

import boardGame.examples.chess.controller.Controller;
import boardGame.examples.chess.model.Checker;
import boardGame.examples.chess.model.Color;
import boardGame.examples.chess.view.View;

public class Launcher {


	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {

		Checker board = new Checker();
		Controller controller = new Controller(board);
		Bot bot = new Bot(board,controller.getHumanColor().getOpposite());
		controller.setBot(bot);
		View view = new View("chess", controller, board);
		if(controller.getHumanColor()==Color.BLACK) {
			controller.botStart();
			System.out.println("bot starts!");
		}
	}
}
