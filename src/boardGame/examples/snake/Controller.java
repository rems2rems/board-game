package boardGame.examples.snake;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import boardGame.api.controller.ControllerObserver;
import boardGame.api.model.Board;
import boardGame.api.model.Direction;
import boardGame.api.model.Pawn;
import boardGame.api.model.Square;
import boardGame.examples.snake.model.Food;
import boardGame.examples.snake.model.Snake;

public class Controller implements ActionListener {

	private Snake snake;
	private Food food;
	private Board board;
	private Timer timer;
	private int timerDelay = 1000;
	
	private ControllerObserver observer;
	
	public Controller(Board board,Snake snake,Food food) {
		this.board = board;
		
		this.snake = snake;

		this.food = food;
		moveFood();
		
		setTimer(getTimerDelay());
	}
	
	public void setObserver(ControllerObserver observer) {
		this.observer = observer;
	}

	public int getTimerDelay() {
		return timerDelay;
	}

	public void setTimerDelay(int timerDelay) {
		this.timerDelay = timerDelay;
	}

	public Timer getTimer() {
		return timer;
	}

	public void setTimer(int delay) {
		if(timer!=null) {
			timer.stop();
		}
		this.timer = new Timer(delay, this);
		timer.start();
	}

	public void setDirection(Direction direction) {
		Square nextSquare = snake.getHead().getSquare().getNeighbor(direction);
		if(snake.getParts().get(1).getSquare() == nextSquare) {
			return;
		}
		snake.setDirection(direction);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Square nextSquare = snake.getHead().getSquare().getNeighbor(snake.getDirection());
		boolean isGameOver = false;
		for(Pawn part : snake.getParts()) {
			if(nextSquare == null || nextSquare.getPawns().contains(part)) {
				isGameOver = true;
				break;
			}
		}
		if(isGameOver) {
			observer.notifyGameIsOver("score:" + snake.getParts().size());
		}
		if(nextSquare.getPawns().contains(food)) {
			snake.eat(food);
			moveFood();
			updateSpeed();
		}
		else {
			snake.move(nextSquare);
		}
		
		System.out.println(board);
	}

	private void moveFood() {
		Square square;
		do {
			square = board.pickRandomSquare();
		}
		while(square.getPawns().size() > 0);
		food.move(square);
	}
	
	private void updateSpeed() {
		int level = 1 + (snake.getParts().size() - 3)/3;
		timerDelay /= level;
		if(timerDelay < 100) {
			timerDelay = 100;
		}
		setTimer(timerDelay);
	}
}
