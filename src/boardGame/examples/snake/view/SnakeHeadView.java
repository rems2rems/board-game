package boardGame.examples.snake.view;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import boardGame.api.model.Pawn;
import boardGame.view.PawnViewImpl;

public class SnakeHeadView extends PawnViewImpl {

	private static final long serialVersionUID = -584057780553767323L;

	public SnakeHeadView(Pawn pawn, int squareHeight,int squareWidth) {
		super(pawn, squareHeight, squareWidth);
	}
	
	{
		try {
			Image sprite = ImageIO.read(new File("images/minesweeper/cursor.png"));
			setSprite(sprite);
		} catch (IOException e) {
			throw new RuntimeException("sprite not found");
		}
	}
}
