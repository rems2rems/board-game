package boardGame.examples.snake.view;

import java.awt.BorderLayout;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import boardGame.api.controller.ControllerObserver;
import boardGame.api.model.Action;
import boardGame.api.model.Board;
import boardGame.api.model.Direction;
import boardGame.api.model.Pawn;
import boardGame.api.model.Square;
import boardGame.api.model.SquareObserver;
import boardGame.api.view.PawnView;
import boardGame.api.view.PlayerObserver;
import boardGame.examples.snake.Controller;
import boardGame.examples.snake.model.Food;
import boardGame.examples.snake.model.Snake;
import boardGame.examples.snake.model.SnakeHead;
import boardGame.examples.snake.model.SnakePart;
import boardGame.view.BoardPanel;
import boardGame.view.SimpleKeyboardManager;
import boardGame.view.SquareViewImpl;

public class View extends JFrame implements ControllerObserver,PlayerObserver,SquareObserver {

	private static final long serialVersionUID = 7435916857686705574L;
	
	private BoardPanel game;
	private Controller controller;
	
	private int squareHeight = 50;
	private int squareWidth = 50;
	
	public View(Controller controller,Board board,Snake snake,Food food) {
		this.controller = controller;
		controller.setObserver(this);
		
		game = new BoardPanel(board, squareHeight, squareWidth);
		for (int row = 0; row < board.getNbRows(); row++) {
			for (int col = 0; col < board.getNbCols(); col++) {
				board.getSquare(row, col).addObserver(this);
				game.addSquareView(new SquareViewImpl(board.getSquare(row, col), squareHeight, squareWidth));
			}
		}
		game.addPawnView(new FoodView(food,squareHeight, squareWidth));
		game.addPawnView(new SnakeHeadView(snake.getHead(), squareHeight, squareWidth));
		for (Iterator<SnakePart> iterator = snake.getParts().listIterator(1); iterator.hasNext();) {
			SnakePart part = iterator.next();
			game.addPawnView(new SnakePartView(part, squareHeight, squareWidth));
			
		}
		
		setLayout(new BorderLayout());
		getContentPane().add(game);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		getContentPane().setSize(game.getSize());
		pack();
		setVisible(true);
		
		SimpleKeyboardManager keyboardManager = new SimpleKeyboardManager();
		addKeyListener(keyboardManager);
		keyboardManager.addObserver(0, this);
	}


	@Override
	public void onAction(int playerNbr, Action action, Direction direction) {
		controller.setDirection(direction);
	}

	@Override
	public void notifyGameIsOver(String message) {
		JOptionPane.showMessageDialog(game,message);
		System.exit(0);
	}

	@Override
	public void onPawnAdded(Square source, Pawn pawn) {
		PawnView pawnView = null;
		if(pawn instanceof Food) {
			pawnView = new FoodView(pawn, squareHeight, squareWidth);
		}
		if(pawn instanceof SnakePart) {
			pawnView = new SnakePartView(pawn, squareHeight, squareWidth);
		}
		if(pawn instanceof SnakeHead) {
			pawnView = new SnakeHeadView(pawn, squareHeight, squareWidth);
		}
		game.addPawnView(pawnView);
	}


	@Override
	public void onPawnRemoved(Square source, Pawn pawn) {
		game.removePawn(pawn);
	}


	@Override
	public void onStateChanged(Square source) {
		
	}
}
