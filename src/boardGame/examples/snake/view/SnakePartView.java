package boardGame.examples.snake.view;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import boardGame.api.model.Pawn;
import boardGame.view.PawnViewImpl;

public class SnakePartView extends PawnViewImpl {

	private static final long serialVersionUID = -1180388783511931687L;

	public SnakePartView(Pawn pawn, int squareHeight,int squareWidth) {
		super(pawn, squareHeight, squareWidth);
	}
	
	{
		try {
			Image sprite = ImageIO.read(new File("images/minesweeper/mine.png"));
			setSprite(sprite);
		} catch (IOException e) {
			throw new RuntimeException("sprite not found");
		}
	}
}
