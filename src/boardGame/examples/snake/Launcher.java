package boardGame.examples.snake;

import boardGame.api.model.Board;
import boardGame.examples.snake.model.Food;
import boardGame.examples.snake.model.Snake;
import boardGame.examples.snake.view.View;
import boardGame.model.BoardImpl;

public class Launcher {

	public static void main(String[] args) {
		int nbRows = 20;
		int nbCols = 20;
		Board board = new BoardImpl(nbRows, nbCols, true, true,false);
		Snake snake = new Snake(board);
		Food food = new Food();
		board.pickRandomSquare().addPawn(food);
		Controller controller = new Controller(board,snake,food);
		
		@SuppressWarnings("unused")
		View view = new View(controller,board,snake,food);
	}
	
	

}
