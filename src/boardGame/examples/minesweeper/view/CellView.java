package boardGame.examples.minesweeper.view;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import boardGame.api.model.Square;
import boardGame.examples.minesweeper.model.Cell;
import boardGame.view.SquareViewImpl;

public class CellView extends SquareViewImpl {

	private static final long serialVersionUID = 6343652949461960249L;
	
	private static Image unknownSprite;
	private static Image mineSprite;
	private static Image flagSprite;
	private static Map<Integer, Image> neighborMinesSprites = new HashMap<>();
	{
		try {
			unknownSprite = ImageIO.read(new File("images/minesweeper/unknown.png"));
			mineSprite = ImageIO.read(new File("images/minesweeper/mine.png"));
			flagSprite = ImageIO.read(new File("images/minesweeper/flagged.png"));
			neighborMinesSprites.put(0, ImageIO.read(new File("images/minesweeper/empty.png")));
			neighborMinesSprites.put(1, ImageIO.read(new File("images/minesweeper/1_neighbor_mines.png")));
			neighborMinesSprites.put(2, ImageIO.read(new File("images/minesweeper/2_neighbor_mines.png")));
			neighborMinesSprites.put(3, ImageIO.read(new File("images/minesweeper/3_neighbor_mines.png")));
			neighborMinesSprites.put(4, ImageIO.read(new File("images/minesweeper/4_neighbor_mines.png")));
			neighborMinesSprites.put(5, ImageIO.read(new File("images/minesweeper/5_neighbor_mines.png")));
			neighborMinesSprites.put(6, ImageIO.read(new File("images/minesweeper/6_neighbor_mines.png")));
			neighborMinesSprites.put(7, ImageIO.read(new File("images/minesweeper/7_neighbor_mines.png")));
			neighborMinesSprites.put(8, ImageIO.read(new File("images/minesweeper/8_neighbor_mines.png")));
		} catch (IOException e) {
			throw new RuntimeException("sprite not found");
		}	
	}
	
	public CellView(Square square,int squareHeight,int squareWidth) {
		super(square,squareHeight,squareWidth);
		
		Image sprite = unknownSprite;
		setSprite(sprite);
	}

	@Override
	public Image getSprite() {
		
		if(getSquare().isKnown()) {
			if(getSquare().isMined()) {
				return mineSprite;
			}
			return neighborMinesSprites.get(getSquare().getNbNeighborMines());
		}
		
		if(getSquare().isFlagged()) {
			return flagSprite;
		}
		return unknownSprite;
		
	}
	
	@Override
	public Cell getSquare() {
		return (Cell)super.getSquare();
	}
}
