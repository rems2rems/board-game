package boardGame.examples.minesweeper.model;

import boardGame.api.model.Pawn;
import boardGame.api.model.Square;
import boardGame.api.model.SquareObserver;
import boardGame.model.SquareImpl;

public class Cell extends SquareImpl {

	private boolean known = false;
	private boolean mined = false;
	private boolean flagged = false;
	
	public Cell(Pawn cursor) {
		super(cursor);
	}
	
	public void toggleFlag(){
		if(!known){
			if(!flagged){
				flagged = true;
			}
			else{
				flagged = false;
			}
			for (SquareObserver observer : observers) {
				observer.onStateChanged(this);
			}
		}
	}
	
	public void reveal() {
		if(isFlagged()){
			return;
		}
		
		setKnown(true);
		
		for (SquareObserver observer : observers) {
			observer.onStateChanged(this);
		}
		
		if(isMined()){
			
			throw new RuntimeException("mined");
		}
		
		int nbFlags = 0;
		for(Square _neighbor: getNeighbors().values()) {
			Cell neighbor = (Cell)_neighbor;
			if(neighbor.isFlagged()) {
				nbFlags += 1;
			}
		}
		
		if(getNbNeighborMines()-nbFlags == 0){
			for (Square n : getNeighbors().values()) {
				Cell neighbor = (Cell)n;
				if(!neighbor.isKnown() && !neighbor.isMined()){
					neighbor.reveal();
				}
			}
		}
	}
	public boolean isKnown() {
		return known;
	}
	public void setKnown(boolean known) {
		this.known = known;
	}
	public boolean isMined() {
		return mined;
	}
	public void setMined(boolean mined) {
		this.mined = mined;
	}
	public int getNbNeighborMines(){
		int nbNeighborMines = 0;
		for (Square n : getNeighbors().values()) {
			Cell neigh = (Cell)n;
			if(neigh.isMined()){
				nbNeighborMines += 1;
			}
		}
		return nbNeighborMines;
	}
	public boolean isFlagged() {
		return flagged;
	}
	public void setFlagged(boolean flagged) {
		this.flagged = flagged;
	}
	
	@Override
	public String toString() {
		
		if(!isKnown()) {
			if(isFlagged()) {
				return "?";
			}
			return "X";
		}
		if(isMined()) {
			return "X";
		}
		return "" + (getNbNeighborMines()==0?" ":getNbNeighborMines());
	}
}
