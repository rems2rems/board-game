package boardGame.examples.minesweeper;

import boardGame.api.model.Board;
import boardGame.examples.minesweeper.model.FieldBuilder;
import boardGame.examples.minesweeper.view.View;

public class Launcher {


	public static void main(String[] args) throws Exception {

		int nbRows = 10;
		int nbCols = 20;
		int nbMines = (int)Math.floor(nbRows*nbCols*0.1);
		FieldBuilder builder = new FieldBuilder(nbRows, nbCols);
		builder.addMines(nbMines);
		Board board = builder.getBoard();
		board.getCursor().move(board.pickRandomSquare());
		Controller controller = new Controller(board);
		
		@SuppressWarnings("unused")
		View game = new View("Demo MineSweeper",controller,board);
	}

}
