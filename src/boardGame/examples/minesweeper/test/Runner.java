package boardGame.examples.minesweeper.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"specs/neighbors.feature","specs/field.feature"}
)
public class Runner {

}
