package boardGame.examples.minesweeper.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.util.HashSet;
import java.util.Set;

import boardGame.api.model.Board;
import boardGame.examples.minesweeper.model.Cell;
import boardGame.examples.minesweeper.model.FieldBuilder;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;


public class FieldTest {

	Board board;
	
	@When("I have a field")
	public void a_field(DataTable table) {
		
		FieldBuilder builder = new FieldBuilder(table.height(),table.width());
		board = builder.getBoard();
		assertNotNull(board);
		Set<Cell> mines = new HashSet<>();
		for (int row = 0; row < table.height(); row++) {
			for (int column = 0; column < table.width(); column++) {
				if(table.cell(row, column).equals("X")) {
					mines.add((Cell)board.getSquare(row, column));
				}
			}
		}
		builder.setMines(mines);		
	}

	@Then("cell at {int} and {int} has {int} neighbor mines")
	public void it_has_neighbor_mines(Integer row, Integer column,Integer neighborMines) {
		
	    Cell cell = (Cell)board.getSquare(row, column);
	    assertSame(neighborMines, cell.getNbNeighborMines());
	}
	
	@Then("cell at {int} and {int} has {int} neighbors")
	public void cell_at_and_has_nbNeighbors_neighbors(Integer row, Integer column,Integer neighbors) {
		
		Cell cell = (Cell)board.getSquare(row, column);
	    assertSame(neighbors, cell.getNeighbors().values().size());
	}
}
