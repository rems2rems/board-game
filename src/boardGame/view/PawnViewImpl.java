package boardGame.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

import boardGame.api.model.Action;
import boardGame.api.model.Pawn;
import boardGame.api.model.Square;
import boardGame.api.view.PawnView;
import boardGame.api.view.PawnViewObserver;

public class PawnViewImpl extends JComponent implements PawnView {

	private static final long serialVersionUID = 4257866731225746528L;
	
	protected Pawn pawn;
	private int squareHeight,squareWidth;
	
	private Color color = Color.BLUE;
	
	private Image sprite;
//	{
//		try {
//			sprite = ImageIO.read(new File("images/minesweeper/cursor.png"));
//		} catch (IOException e) {
//			throw new RuntimeException("sprite not found");
//		}	
//	}

	protected List<PawnViewObserver> observers = new ArrayList<>();

	public PawnViewImpl(Pawn pawn,int squareHeight,int squareWidth) {
			
		this.squareHeight = squareHeight;
		this.squareWidth = squareWidth;	
		this.pawn = pawn;
		pawn.addObserver(this);
	}
	
	@Override
	public Image getSprite() {
		return sprite;
	}

	@Override
	public Pawn getPawn() {
		return pawn;
	}
	
	@Override
	public void onAction(Pawn source, Action action) {
		for (PawnViewObserver observer : observers) {
			observer.onChange(this);
		}
	}
	
	@Override
	public void onMove(Pawn source, Square from, Square to) {
		for (PawnViewObserver observer : observers) {
			observer.onChange(this);
		}
	}
	
	
	@Override
	public void addObserver(PawnViewObserver observer) {
		observers.add(observer);
	}

	@Override
	public void removeObserver(PawnViewObserver observer) {
		observers.remove(observer);
	}

	@Override
	public boolean hasSprite() {
		return sprite!=null;
	}

	@Override
	public Color getColor() {
		return color;
	}

	@Override
	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public void setSprite(Image sprite) {
		this.sprite = sprite;
	}
	
	@Override
	public void paint(Graphics g) {
		Graphics2D graphics = (Graphics2D)g;
//		try {
			graphics.drawImage(getSprite(),getPawn().getSquare().getColumn()*squareWidth,getPawn().getSquare().getRow()*squareHeight,squareWidth,squareHeight,this);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
	}
}
