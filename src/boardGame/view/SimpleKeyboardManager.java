package boardGame.view;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import boardGame.api.model.Action;
import boardGame.api.model.Direction;
import boardGame.api.view.PlayerObserver;

/**
 * 
 * simple keyboard mappings:
 * for first player (id=0):
 * 		MOVES = UP,LEFT,DOWN,RIGHT
 * 		ACTION1 = SPACE
 * 		ACTION2 = ALTGR
 * 		ACTION3 = CONTROL(RIGHT)
 * for second player (id=1):
 * 		MOVES = Z,Q,S,D (french keyboard mapping)
 * 		ACTION1 = SHIFT(LEFT)
 * 		ACTION2 = CONTROL(LEFT)
 * 		ACTION3 = ALT
 *
 */
public class SimpleKeyboardManager implements KeyListener{

	private Map<Integer,Set<PlayerObserver>> allObservers = new HashMap<>();
	private Set<Integer> player0Moves = new HashSet<>();
	private Set<Integer> player1Moves = new HashSet<>();
	
	public SimpleKeyboardManager() {
		player0Moves.add(KeyEvent.VK_UP);
		player0Moves.add(KeyEvent.VK_RIGHT);
		player0Moves.add(KeyEvent.VK_DOWN);
		player0Moves.add(KeyEvent.VK_LEFT);
		player1Moves.add(KeyEvent.VK_Z);
		player1Moves.add(KeyEvent.VK_Q);
		player1Moves.add(KeyEvent.VK_S);
		player1Moves.add(KeyEvent.VK_D);
	}
	
	public void addObserver(int playerNbr,PlayerObserver observer){
		Set<PlayerObserver> observers = allObservers.get(playerNbr);
		if(observers==null){
			observers = new HashSet<>();
			allObservers.put(playerNbr, observers );
		}
		observers.add(observer);
	}
	
	public void removeObserver(int playerNbr,PlayerObserver observer){
		allObservers.get(playerNbr).remove(observer);
	}

	@Override
	public void keyPressed(KeyEvent event) {
		int key = event.getKeyCode();
		int location = event.getKeyLocation();
		if(key == KeyEvent.VK_SPACE){
			for (PlayerObserver observer : allObservers.get(0)) {
				observer.onAction(0, Action.ACTION1, null);
			}
			return;
		}
		if(key == KeyEvent.VK_SHIFT && location == KeyEvent.KEY_LOCATION_RIGHT){
			for (PlayerObserver observer : allObservers.get(0)) {
				observer.onAction(0, Action.ACTION2, null);
			}
			return;
		}
		if(key == KeyEvent.VK_CONTROL && location == KeyEvent.KEY_LOCATION_RIGHT){
			for (PlayerObserver observer : allObservers.get(0)) {
				observer.onAction(0, Action.ACTION3, null);
			}
			return;
		}
		if(player0Moves.contains(key)){
			Direction direction = keyCodeToDirection(key);
			for (PlayerObserver observer : allObservers.get(0)) {
				observer.onAction(0, Action.MOVE, direction);
			}
			return;
		}
		if(key == KeyEvent.VK_SHIFT && location == KeyEvent.KEY_LOCATION_LEFT){
			if(allObservers.get(1)==null) {
				System.out.println("Player 2 is not playing...");
				return;
			}
			for (PlayerObserver observer : allObservers.get(1)) {
				observer.onAction(1, Action.ACTION2, null);
			}
			return;
		}
		if(key == KeyEvent.VK_CONTROL && location == KeyEvent.KEY_LOCATION_LEFT){
			if(allObservers.get(1)==null) {
				System.out.println("Player 2 is not playing...");
				return;
			}
			for (PlayerObserver observer : allObservers.get(1)) {
				observer.onAction(1, Action.ACTION3, null);
			}
			return;
		}
		if(key == KeyEvent.VK_ALT && location == KeyEvent.KEY_LOCATION_LEFT){
			if(allObservers.get(1)==null) {
				System.out.println("Player 2 is not playing...");
				return;
			}
			for (PlayerObserver observer : allObservers.get(1)) {
				observer.onAction(1, Action.ACTION1, null);
			}
			return;
		}
		if(player1Moves.contains(key)){
			if(allObservers.get(1)==null) {
				System.out.println("Player 2 is not playing...");
				return;
			}
			Direction direction = keyCodeToDirection(key);
			for (PlayerObserver observer : allObservers.get(1)) {
				observer.onAction(1, Action.MOVE, direction);
			}
			return;
		}
		
	}
	
	private Direction keyCodeToDirection(int keyCode){
		switch (keyCode) {
			case KeyEvent.VK_UP:
			case KeyEvent.VK_Z:
				return Direction.UP;
			case KeyEvent.VK_RIGHT:
			case KeyEvent.VK_D:
				return Direction.RIGHT;
			case KeyEvent.VK_DOWN:
			case KeyEvent.VK_S:
				return Direction.DOWN;
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_Q:
				return Direction.LEFT;
		}
		throw new RuntimeException("not a direction key");
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// unused
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// unused
		
	}

}
