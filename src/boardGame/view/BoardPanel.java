package boardGame.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JPanel;

import boardGame.api.model.Board;
import boardGame.api.model.Pawn;
import boardGame.api.view.BoardView;
import boardGame.api.view.PawnView;
import boardGame.api.view.SquareView;
import boardGame.model.BoardImpl;

@SuppressWarnings("serial")
public class BoardPanel extends JPanel implements BoardView {

	Board board = new BoardImpl();
	private SquareView[][] squares;
	private Map<Pawn, PawnView> pawnsViews = new HashMap<>();
	
	private int squareWidth;
	private int squareHeight;

	//public BoardPanel(int nbRows,int nbCols,int squareWidth,int squareHeight) {
	public BoardPanel(Board board,int squareWidth,int squareHeight) {
		super();
		this.board = board;
		this.squareHeight = squareHeight;
		this.squareWidth = squareWidth;
		setSize(new Dimension(squareWidth*board.getNbCols(), squareHeight*board.getNbRows()));
		setMinimumSize(getSize());
		setMaximumSize(getSize());
                setPreferredSize(getSize());
		setBackground(Color.DARK_GRAY);
		
		squares = new SquareView[board.getNbRows()][board.getNbCols()];
//		for (int row = 0;row< squares.length;row++) {
//			for (int col = 0;col< squares[0].length;col++) {
//				squares[row][col] = new SquareViewImpl(null);
//			}
//		}
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D graphics = (Graphics2D)g;
		for (int row = 0;row< squares.length;row++) {
			for (int col = 0;col< squares[0].length;col++) {
				
				JComponent comp = (JComponent)squares[row][col];
				comp.paint(graphics);
//				graphics.setPaint(squares[row][col].getColor());
//				graphics.fillRect(col*squareWidth,row*squareHeight,squareWidth,squareHeight);
//				
//				if(squares[row][col].getSprite()!=null) {
//					graphics.drawImage(squares[row][col].getSprite(),col*squareWidth,row*squareHeight,squareWidth,squareHeight,this);
//				}
//				
//				for (Pawn pawn : squares[row][col].getSquare().getPawns()) {
////					try {
//						graphics.drawImage(pawnsViews.get(pawn).getSprite(),col*squareWidth,row*squareHeight,squareWidth,squareHeight,this);
////					} catch (Exception e) {
////						e.printStackTrace();
////					}
//				}

				for (Pawn pawn : squares[row][col].getSquare().getPawns()) {
					comp = (JComponent)pawnsViews.get(pawn);
					comp.paint(graphics);
				}
			}
		}
		if(board.hasCursor()) {
			Pawn cursor = board.getCursor();
			graphics.drawImage(pawnsViews.get(cursor).getSprite(),cursor.getSquare().getColumn()*squareWidth,cursor.getSquare().getRow()*squareHeight,squareWidth,squareHeight,this);
		}
	}

	@Override
	public void onChange(SquareView squareView) {
		repaint();
	}

	@Override
	public void onChange(PawnView pawnView) {
		repaint();
	}

	@Override
	public void addPawnView(PawnView pawnView) {
		pawnsViews.put(pawnView.getPawn(), pawnView);
		pawnView.addObserver(this);
	}

	@Override
	public void addSquareView(SquareView squareView) {
		squares[squareView.getSquare().getRow()][squareView.getSquare().getColumn()] = squareView;
		squareView.addObserver(this);
	}

	@Override
	public void removePawn(Pawn pawn) {
		pawnsViews.get(pawn).removeObserver(this);
		pawnsViews.remove(pawn);
	}
}
