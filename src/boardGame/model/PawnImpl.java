package boardGame.model;

import java.util.HashSet;
import java.util.Set;

import boardGame.api.model.Direction;
import boardGame.api.model.Pawn;
import boardGame.api.model.PawnObserver;
import boardGame.api.model.Square;

public class PawnImpl implements Pawn {

	private Square square;
	private Set<PawnObserver> observers = new HashSet<>();

	@Override
	public Square getSquare() {
		return square;
	}

	@Override
	public void setSquare(Square square) {
		this.square = square;
	}

	@Override
	public void move(Direction direction) {
		move(square.getNeighbor(direction));
	}

	@Override
	public void addObserver(PawnObserver observer) {
		observers.add(observer);
	}

	@Override
	public void removeObserver(PawnObserver observer) {
		observers.remove(observer);
	}

	@Override
	public void move(Square to) {
		square.removePawn(this);
		to.addPawn(this);
		
		for (PawnObserver observer : observers) {
			observer.onMove(this, square, to);
		}
	}
	
	@Override
	public String toString() {
		return ".";
	}
}
