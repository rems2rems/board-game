
Feature: MineSweeper
  I want to test the minesweeper field

	Scenario Outline: count neighbors
    When I have a field
      | _ | _ | _ |
      | _ | _ | _ |
      | _ | _ | _ |
      | _ | _ | _ |
    Then cell at <row> and <column> has <nbNeighbors> neighbors
    
    Examples:
    	| row | column | nbNeighbors |
    	| 0   | 0      | 3       |
    	| 0   | 1      | 5       |
    	| 0   | 2      | 3       |
    	| 1   | 0      | 5       |
    	| 1   | 1      | 8       |
    	| 1   | 2      | 5       |
    	| 2   | 0      | 5       |
    	| 2   | 1      | 8       |
    	| 2   | 2      | 5       |
    	| 3   | 0      | 3       |
    	| 3   | 1      | 5       |
    	| 3   | 2      | 3       |
    	